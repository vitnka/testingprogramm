package itis.quiz.spaceships;


import java.util.ArrayList;
import java.util.Comparator;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager{


        @Override
        public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
            ArrayList<Integer> list = new ArrayList<>();
            for (Spaceship spaceship : ships) {
                list.add(spaceship.getFirePower());
            }
            list.sort(Comparator.reverseOrder());
            for (Spaceship spaceship : ships) {
                if (spaceship.getFirePower() == list.get(0)) {
                    return spaceship;
                }
            }
            return null;
        }

        @Override
        public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
            for (Spaceship spaceship : ships){
                if (spaceship.getName().equals(name)) {
                    return spaceship;
                }
            }
            return null;
        }

        @Override
        public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
            ArrayList <Spaceship> list = new ArrayList<>();
            for (Spaceship spaceship : ships){
                if (spaceship.getCargoSpace() >= cargoSize)
                    list.add(spaceship);
            }
            return list;
        }

        @Override
        public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
            ArrayList <Spaceship> list = new ArrayList<>();
            for (Spaceship spaceship : ships){
                if (spaceship.getFirePower() == 0)
                    list.add(spaceship);
            }
            return list;
        }
    }

