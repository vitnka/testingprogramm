package itis.quiz.spaceships;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;


public class SpaceShipFleetManagerTestWithLibrary {

    static SpaceshipFleetManager center = new CommandCenter();
    static boolean flag = false;
    ArrayList<Spaceship> list = new ArrayList<>();

    @AfterEach
    void afterEach(){
        list.clear();
        flag = false;
    }

    @Test
    @DisplayName("Возвращает корабль, который имеет введенное имя")
    void getShipByName_shipsWithRightNameExists_returnTargetShip() {
        list.add(new Spaceship("First", 10, 0, 0));
        list.add(new Spaceship("Second", 10, 10, 5));
        list.add(new Spaceship("Third", 10, 40, 0));
        String testName = "Second";
        Spaceship shipsWithRightName = center.getShipByName(list, "Second");
        if (shipsWithRightName != null && shipsWithRightName.getName().equals(testName)){
            flag = !flag;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает null,если не существует корабля с нужным именем")
    void getShipByName_shipsWithRightNameNotExists_returnNull(){
        list.add(new Spaceship("First",  10,0,0));
        list.add(new Spaceship("Second", 10 ,10,5));
        list.add(new Spaceship("Third", 10,40,0));
        Spaceship shipsWithRightName = center.getShipByName(list,"Marlin");
        if (shipsWithRightName == null) {
            flag = !flag;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает null, если существует корабль с боевой мощью")
    void getAllCivilianShips_shipsWithFirePowerExists_returnNullList(){
        list.add(new Spaceship("First",  10,0,0));
        list.add(new Spaceship("Second", 10 ,10,5));
        list.add(new Spaceship("Third", 10,40,0));
        ArrayList<Spaceship> mostPowerfulShip = center.getAllCivilianShips(list);
        if (mostPowerfulShip.isEmpty()) {
            flag = !flag;
        }
        assertTrue(flag);

    }

    @Test
    @DisplayName("Возвращает список мирных кораблей")
    void getAllCivilianShips_shipsWithoutFirePowerExists_returnListOfShips(){
        list.add(new Spaceship("First",  0,0,0));
        list.add(new Spaceship("Second", 0 ,10,5));
        list.add(new Spaceship("Third", 0,40,0));
        ArrayList<Spaceship> shipsWithoutFirePower= center.getAllCivilianShips(list);
        if (!shipsWithoutFirePower.isEmpty()) {
            flag = !flag;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает null писок, если у кораблей не хватает вместимости")
    void getAllShipsWithEnoughCargoSpace_shipsWithEnoughCargoSizeNotExists_returnNullList(){
        list.add(new Spaceship("First", 15 ,0,5));
        list.add(new Spaceship("Second", 19 ,10,5));
        list.add(new Spaceship("Third", 19 ,40,5));
        ArrayList<Spaceship> shipsWithEnoughCargo = center.getAllShipsWithEnoughCargoSpace(list,50);
        if (shipsWithEnoughCargo.isEmpty() ) {
            flag = !flag;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает список кораблей у которых хватает вместимости")
    void getAllShipsWithEnoughCargoSpace_shipsWithEnoughCargoSizeExists_returnListOfShips() {
        list.add(new Spaceship("First", 15, 0, 5));
        list.add(new Spaceship("Second", 19, 10, 5));
        list.add(new Spaceship("Third", 19, 50, 5));
        ArrayList<Spaceship> shipsWithEnoughCargoSize = center.getAllShipsWithEnoughCargoSpace(list, 50);
        if (!shipsWithEnoughCargoSize.isEmpty()) {
            flag = !flag;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает первый в списке корабль с наибольшей боевой мощью")
    void getMostPowerfulShip_shipWithMostPowerExists_returnFirstShip(){
        list.add(new Spaceship("First", 15 ,0,5));
        list.add(new Spaceship("Second", 19 ,0,5));
        list.add(new Spaceship("Third", 19 ,0,5));
        String testName = "Second";
        Spaceship shipWithMostPower = center.getMostPowerfulShip(list);
        if ( shipWithMostPower!= null && shipWithMostPower.getFirePower() == 19 && shipWithMostPower.getName().equals(testName)) {
            flag =!flag;
        }
        assertTrue(flag);

    }

    @Test
    @DisplayName("Возвращает корабль с самой большой боевой мощью")
    void getMostPowerfulShip_shipWithMostPowerExists_returnShip(){
        list.add(new Spaceship("First", 15 ,0,5));
        list.add(new Spaceship("Second", 10 ,0,5));
        list.add(new Spaceship("Third", 19 ,0,5));
        Spaceship shipWithMostPower = center.getMostPowerfulShip(list);
        if ( shipWithMostPower != null && shipWithMostPower.getFirePower() == 19) {
            flag =!flag;
        }
        assertTrue(flag);
    }

    @Test
    @DisplayName("Возвращает null, если нет боевого корабля")
    void getMostPowerfulShip_shipWithMostPowerNotExists_returnNull(){
        list.add(new Spaceship("First", 0 ,0,5));
        list.add(new Spaceship("Second", 0 ,0,5));
        list.add(new Spaceship("Third", 0 ,0,5));
        Spaceship shipWithMostPower = center.getMostPowerfulShip(list);
        if (shipWithMostPower == null || shipWithMostPower.getFirePower() == 0) {
            flag = !flag;
        }
        assertTrue(flag);

    }

}
