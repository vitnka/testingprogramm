package itis.quiz.spaceships;

import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class SpaceShipFleetManagerTest {
    SpaceshipFleetManager center; // Метод надо у чего-то вызывать, так как проверяем интерфейс,переменная его типа.
    ArrayList<Spaceship> list = new ArrayList<>();

    public static void main (String[] args){
        SpaceShipFleetManagerTest test = new SpaceShipFleetManagerTest(new CommandCenter());


        boolean test1Method1 = test.getMostPowerfulShip_shipWithMostPowerExists_returnShip();
        if (test1Method1){
            System.out.println("All good!");
        } else {
            System.out.println("Test 1 of the 1st method failed!");
        }
        boolean test2Method1 = test.getMostPowerfulShip_shipWithMostPowerNotExists_returnNull();
        if (test2Method1){
            System.out.println("All good!");
        } else {
            System.out.println("Test 2 of the 1st method failed!");
        }
        boolean test3Method1 = test.getMostPowerfulShip_shipWithMostPowerExists_returnFirstShip();
        if (test3Method1){
            System.out.println("All good!");
        } else {
            System.out.println("Test 3 of the 1st method failed!");
        }


        boolean method1 = test1Method1 && test2Method1 && test3Method1;


        boolean test1Method2 = test.getShipByName_shipsWithRightNameExists_returnTargetShip();
        if (test1Method2){
            System.out.println("All good!");
        } else {
            System.out.println("test1 of the 2nd method failed!");
        }
        boolean test2Method2 = test.getShipByName_shipsWithRightNameNotExists_returnNull();
        if (test2Method2){
            System.out.println("All good!");
        } else {
            System.out.println("test2 of the 2nd method failed!");
        }


        boolean method2 = test1Method2 && test2Method2;


        boolean test1Method3 = test.getAllShipsWithEnoughCargoSpace_shipsWithEnoughCargoSizeExists_returnListOfShips();
        if (test1Method3){
            System.out.println("All good!");
        } else {
            System.out.println("test1 of the 3rd method failed!");
        }
        boolean test2Method3 = test.getAllShipsWithEnoughCargoSpace_shipsWithEnoughCargoSizeNotExists_returnNullList();
        if (test2Method3){
            System.out.println("All good!");
        } else {
            System.out.println("test2 of the 3rd method failed!");
        }


        boolean method3 = test1Method3 && test2Method3;


        boolean test1Method4 = test.getAllCivilianShips_shipsWithoutFirePowerExists_returnListOfShips();
        if (test1Method4){
            System.out.println("All good!");
        } else {
            System.out.println("test1 of the 4th method failed!");
        }
        boolean test2Method4 = test.getAllCivilianShips_shipsWithFirePowerExists_returnNullList();
        if (test2Method4){
            System.out.println("All good!");
        } else {
            System.out.println("test2 of the 4th method failed!");
        }


        boolean method4 = test1Method4 && test2Method4;


        int result = test.getPoints(method1) + test.getPoints(method2) + test.getPoints(method3) + test.getPoints(method4);
        System.out.println("Your result is: "+ result);

    }

    private int getPoints(boolean result){
        if (result) {
            return 1;
        }
        return 0;
    }


    private boolean getShipByName_shipsWithRightNameExists_returnTargetShip(){
        list.add(new Spaceship("First",  10,0,0));
        list.add(new Spaceship("Second", 10 ,10,5));
        list.add(new Spaceship("Third", 10,40,0));
        String testName = "Second";
        Spaceship temp = center.getShipByName(list,"Second");
        if (temp != null && temp.getName().equals(testName)){
            list.clear();
            return true;
        }
        list.clear();
        return false;
    }

    private boolean getShipByName_shipsWithRightNameNotExists_returnNull(){
        list.add(new Spaceship("First",  10,0,0));
        list.add(new Spaceship("Second", 10 ,10,5));
        list.add(new Spaceship("Third", 10,40,0));
        Spaceship temp = center.getShipByName(list,"Marlin");
        if (temp == null){
            list.clear();
            return true;
        }
        list.clear();
        return false;
    }

    private boolean getAllCivilianShips_shipsWithFirePowerExists_returnNullList(){
        list.add(new Spaceship("First",  10,0,0));
        list.add(new Spaceship("Second", 10 ,10,5));
        list.add(new Spaceship("Third", 10,40,0));
        ArrayList<Spaceship> temp = center.getAllCivilianShips(list);
        if (temp.size() == 0 ) {
            list.clear();
            return true;
        }
        list.clear();
        return false;
    }

    private boolean getAllCivilianShips_shipsWithoutFirePowerExists_returnListOfShips(){
        list.add(new Spaceship("First",  0,0,0));
        list.add(new Spaceship("Second", 0 ,10,5));
        list.add(new Spaceship("Third", 0,40,0));
        ArrayList<Spaceship> temp = center.getAllCivilianShips(list);
        if (temp.size() != 0 ) {
            list.clear();
            return true;
        }
        list.clear();
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_shipsWithEnoughCargoSizeNotExists_returnNullList(){
        list.add(new Spaceship("First", 15 ,0,5));
        list.add(new Spaceship("Second", 19 ,10,5));
        list.add(new Spaceship("Third", 19 ,40,5));
        ArrayList<Spaceship> temp = center.getAllShipsWithEnoughCargoSpace(list,50);
        if (temp.size() == 0 ) {
            list.clear();
            return true;
        }
        list.clear();
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_shipsWithEnoughCargoSizeExists_returnListOfShips(){
        list.add(new Spaceship("First", 15 ,0,5));
        list.add(new Spaceship("Second", 19 ,10,5));
        list.add(new Spaceship("Third", 19 ,50,5));
        ArrayList<Spaceship> temp = center.getAllShipsWithEnoughCargoSpace(list,50);
        if (temp.size() != 0 ) {
            list.clear();
            return true;
        }
        list.clear();
        return false;
    }

    private boolean getMostPowerfulShip_shipWithMostPowerExists_returnFirstShip(){
        list.add(new Spaceship("First", 15 ,0,5));
        list.add(new Spaceship("Second", 19 ,0,5));
        list.add(new Spaceship("Third", 19 ,0,5));
        String testName = "Second";
        Spaceship temp = center.getMostPowerfulShip(list);
        if ( temp != null && temp.getFirePower() == 19 && temp.getName().equals(testName)) {
            list.clear();
            return true;
        }
        list.clear();
        return false;
    }

    private boolean getMostPowerfulShip_shipWithMostPowerExists_returnShip(){
        list.add(new Spaceship("First", 15 ,0,5));
        list.add(new Spaceship("Second", 10 ,0,5));
        list.add(new Spaceship("Third", 19 ,0,5));
        Spaceship temp = center.getMostPowerfulShip(list);
        if ( temp != null && temp.getFirePower() == 19) {
            list.clear();
            return true;
        }
        list.clear();
        return false;
    }

    private boolean getMostPowerfulShip_shipWithMostPowerNotExists_returnNull(){
        list.add(new Spaceship("First", 0 ,0,5));
        list.add(new Spaceship("Second", 0 ,0,5));
        list.add(new Spaceship("Third", 0 ,0,5));
        Spaceship temp = center.getMostPowerfulShip(list);
        if (temp == null || temp.getFirePower() == 0){
            list.clear();
            return true;
        }
        list.clear();
        return false;
    }

    public SpaceShipFleetManagerTest(SpaceshipFleetManager center) {
        this.center = center;
    }

}
